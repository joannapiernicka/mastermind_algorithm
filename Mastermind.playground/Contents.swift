//: Playground - noun: a place where people can play
//  MasterMind ver. 1.2
//  Created by Joanna Charysz-Piernicka on 21.01.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.


import UIKit

class Mastermind {


    let colours = ["red", "blue", "green", "yellow", "orange", "purple", "brown", "pink"]
    var codeLenght = Int()
    var maxNumberOfTurn = Int()
    var turn = Int()

    var coloursCodeArray = [String]()
    var coloursPlayersArray = [String]()
    var secondEtapArray = [String]()

    var isPlaceOk = 0


    //MARK: randoming single colour
    func randomColour()->String {
        let colourIndex = Int(arc4random()) % colours.count
        return colours[colourIndex]
    }


    //MARK: randoming code
    func randomCode(codeLenght: Int?) -> [String] {
        while coloursCodeArray.count < codeLenght! {
            coloursCodeArray.append(randomColour())
        }
        print("Kod do zgadniecia:\(coloursCodeArray)")
        return coloursCodeArray
    }


    //MARK: checking colour and place
    func check(_ a: [String])  {
        isPlaceOk = 0
        var isOnlyColourOk = 0
        var indexTable = 0
        for _ in a {
            if coloursCodeArray[indexTable] == a[indexTable] {
                isPlaceOk += 1
            
            } else {
                if coloursCodeArray.contains(a[indexTable]) {
                isOnlyColourOk += 1
                } else {
                }
            }
            indexTable += 1
        }
    print("Runda \(maxNumberOfTurn - turn)")
    print("czarne: \(isPlaceOk)")
    print("białe: \(isOnlyColourOk)")
    print("zostalo Ci \(turn) ruchow")
        
    stateGame(isPlaceOk: isPlaceOk)
    turn -= 1
    }

    //MARK: state of game winner/loser
    
    func stateGame(isPlaceOk: Int) {
        if isPlaceOk == codeLenght && turn >= 0 {
            print("Wygrałes!")
            exit(0)
            
        } else if isPlaceOk != codeLenght && turn > 0 {
            print("Sprobuj jeszcze raz")
            
        } else {
            print("Przegrałeś :(")
            exit(0)
        }
    }
    
    //MARK: fill the array with the last color
    
    func fillArrayLastColor() {
        while secondEtapArray.count < codeLenght {
            secondEtapArray.append(colours[colours.count-1])
        }
    }
    
    //MARK: generate permutions
    
    func permuteWirth(_ a: [String], _ n: Int) {
        if n == 0 {
            print("Gracz: \(a)")// display the current permutation
            coloursPlayersArray = a
            check(coloursPlayersArray)
        } else {
            var a = a
            permuteWirth(a, n - 1)
            for i in 0..<n {
                swap(&a[i], &a[n])
                permuteWirth(a, n - 1)
                swap(&a[i], &a[n])
            }
        }
    }

    //MARK: course of the game

    func play(maxNumberOfTurn: Int, codeLenght: Int) {
        self.maxNumberOfTurn = maxNumberOfTurn
        self.codeLenght = codeLenght
        
        turn = maxNumberOfTurn
        randomCode(codeLenght: codeLenght)

        var indexTable = 0
        let firstEtap = colours.count - 1
    
        //I Etap: We determine whether a given color is included in the code to guess
        for _ in 1...firstEtap where secondEtapArray.count < codeLenght {
            while coloursPlayersArray.count < codeLenght {
                coloursPlayersArray.append(colours[indexTable])
            }
            print("Gracz: \(coloursPlayersArray)")
            check(coloursPlayersArray)
            
            // if the color is in the code we add it to the array on which will do permutations
            while isPlaceOk > 0 {
                secondEtapArray.append(colours[indexTable])
                isPlaceOk -= 1
            }
            indexTable += 1
            coloursPlayersArray.removeAll()
            print("tablica-slownik \(secondEtapArray)")
    }
    
        //fill the array with the last color
        fillArrayLastColor()
        print("na tej tablicy bedziemy robic permutacje\(secondEtapArray)")

        //II Etap: permutations
        permuteWirth(secondEtapArray, secondEtapArray.count - 1)
    }
    
 

   init() {
        play(maxNumberOfTurn: 30, codeLenght: 4)
    }
}

Mastermind.init()







